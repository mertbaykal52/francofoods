const path = require("path");

module.exports = {
    entry: ["./src/js/main.js", "./src/sass/common.scss"],
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "assets/js"),
    },
    module: {
        rules: [{
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    "style-loader",
                    {
                        loader: "file-loader",
                        options: {
                            name: "../css/common.css",
                        },
                    },
                    {
                        loader: "extract-loader",
                    },
                    {
                        loader: "css-loader?url=false",
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                    },
                },
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: "[name][hash].[ext]",
                        outputPath: "fonts/",
                    },
                }, ],
            },
        ],
    },
    watch: true,
};