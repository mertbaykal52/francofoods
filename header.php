<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css">
    <link href="assets/css/common.css" rel="stylesheet" />


    <title>Franco Foods</title>


</head>

<body>
    <script>
        $(document).ready(function() {
            const mobileMenu = $('.mobile-menu');
            if (mobileMenu) {
                $('.toggle').click(function() {
                    mobileMenu.addClass('active');
                    var x = document.getElementById("myDIV");
                   

                    x.style.display = "none";
                   
                })
                $('.mobile-menu__close button').click(function() {
                    mobileMenu.removeClass('active');
                    var x = document.getElementById("myDIV");
                   

                    x.style.display = "none";
                   
                });
            }
        });
    </script>
     <script>
function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
    <header>
        <nav>
            <div class="mobile-menu">
                <div class="mobile-menu__close">
                    <button type="button">X</button>
                </div>
                <div>
                    <img src="assets/images/franco-foods-logo.png" alt="franco foods logo">
                </div>
                <ul>

                    <li><a href="/">HOME PAGE</a></li>
                    <li><a href="about-us">ABOUT US</a></li>
                    <li onclick="myFunction()"><a href="#">PRODUCTS</a></li>
                    <div id="myDIV">
                        <li><a href="standard-moistured">Dried Tomatoes</a></li>
                        <li><a href="dried-fruits">Dried Fruits</a></li>
                        <li><a href="spices-and-herbs">Spices And Herbs</a></li>

                    </div>
                    <li><a href="contact">CONTACT</a></li>






                </ul>
            </div>
        </nav>
    </header>
    <header>
        <div class="header">


            <div class="container-header">
                <div class="header-wrapper">
                    <div class="header__logo">
                        <a href="/">
                            <img src="assets/images/franco-foods-logo.png" alt="franco-foods logo">
                        </a>
                    </div>
                    <div class="header__all">

                        <div class="header__social-all">

                            <div class="header__social-langs">
                                <div class="header__social-icon">
<a href="https://www.linkedin.com/company/franco-foods-s-a"><img src="assets/images/linkedin-icon.png" alt="linkedin iconu"></a>
                                    


                                </div>
                                <div class="header__social-lang">

                                    <div class="header__social-infos_desc">

                                        <p class="header-info no-border lang">TR</p>

                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="header__menu">
                            <div class="toggle">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </div>

                            <ul class="menu-list">
                                <li class="menu-list__item active">
                                    <a href="/">HOME PAGE</a>
                                    <!-- <ul class="menu-drop ilk">
                            <li class="menu-list__item menu_dropdown">

                                <a href="/about-us">Hakkımızda</a>
                            </li>
                            <li class="menu-list__item menu_dropdown">

                                <a href="/views/corporate/manifesto">Manifesto</a>

                            </li>
                            <li class="menu-list__item menu_dropdown">

                                <a href="/views/corporate/our-goals">Hedeflerimiz</a>

                            </li>
                            <li class="menu-list__item menu_dropdown">


                                <a href="/views/corporate/sustainabilitiy">Sürdürülebilirlik</a>

                            </li>
                            <li class="menu-list__item menu_dropdown">

                                <a href="/views/corporate/board-of-management">Yönetim Kurulu</a>

                            </li>
                            <li class="menu-list__item menu_dropdown">

                                <a href="/views/corporate/human-resources">İnsan Kaynakları</a>

                            </li>




                        </ul> -->
                                </li>
                                <li class="menu-list__item">

                                    <a href="about-us">ABOUT US</a>

                                    <!-- <ul class="menu-drop ilk">
                            <li class="menu-list__item menu_dropdown">
                                <a href="/views/brands/mezzemarin">mezzeMarin</a>
                            </li>
                            <li class="menu-list__item menu_dropdown">
                                <a href="/views/brands/biocampus">BioCampus</a>
                            </li>

                        </ul> -->
                                </li>

                                <li class="menu-list__item">

                                    <a href="#">PRODUCTS</a>
                                    <ul class="menu-drop ilk">
                                        <li class="menu-list__item menu_dropdown">
                                            <a href="standard-moistured">Dried Tomatoes</a>
                                        </li>
                                        <li class="menu-list__item menu_dropdown">
                                            <a href="dried-fruits">Dried Fruits</a>
                                        </li>
                                        <li class="menu-list__item menu_dropdown">
                                            <a href="spices-and-herbs">Spices And Herbs</a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="menu-list__item">

                                    <a href="contact">CONTACT</a>


                                </li>

                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>