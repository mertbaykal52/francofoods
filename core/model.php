<?php


class FormError
{
    protected $ERROR_TYPES = array(
        'REQUIRED' => 'Bu alan zorunlu',
        'MIN_LENGTH' => 'En az %d karakter olmalı',
        'MAX_LENGTH' => 'En fazla %d karakter olmalı',
        'MIN_VALUE' => 'Değer en az %d olabilir',
        'MAX_VALUE' => 'Değer en fazla %d olabilir',
        'TYPE_INT' => 'Sadece numeric değerler girebilirsiniz',
        'TYPE_FLOAT' => 'Sadece küsürlü değerler girebilirsiniz',
        'TYPE_BOOL' => 'Yalnızca evet/hayır şeklinde değer girebilirsiniz',
        'TYPE_FILE' => 'Bozuk veya geçersiz dosya',
        'FILE_EXTENSIONS' => 'Yalnızca %s tipinde dosyalar ekleyebilirsiniz',
    );
}


class FormType
{
    public static function required(bool $value = true)
    {
        return array('type' => 'required', 'value' => $value);
    }
    public static function minlength(string $value)
    {
        return array('type' => 'minlength', 'value' => $value);
    }
    public static function maxlength(string $value)
    {
        return array('type' => 'maxlength', 'value' => $value);
    }
    public static function minvalue(int $value)
    {
        return array('type' => 'minvalue', 'value' => $value);
    }
    public static function maxvalue(int $value)
    {
        return array('type' => 'maxvalue', 'value' => $value);
    }
    public static function typeString()
    {
        return array('type' => 'string');
    }
    public static function typeInt()
    {
        return array('type' => 'int');
    }
    public static function typeFloat()
    {
        return array('type' => 'float');
    }
    public static function typeBool()
    {
        return array('type' => 'bool');
    }
    public static function typeFile(bool $multiple = false, array $extensions = array())
    {
        return array('type' => 'file', 'multiple' => $multiple, 'extensions' => $extensions);
    }
}

class Model extends FormError
{
    private $SCHEMA = [];
    private $ERRORS = [];
    private $DATAS = [];
    private $FILES = [];

    public function getErrors()
    {
        return $this->ERRORS;
    }

    public function getDatas()
    {
        return $this->DATAS;
    }
    public function getFiles()
    {
        return $this->FILES;
    }

    public function set(string $name, array $properties = array(), array $errors = array())
    {
        array_push($this->SCHEMA, array('name' => $name, 'props' => $properties, 'errors' => $errors));
    }

    public function valid()
    {
        foreach ($this->SCHEMA as $item) {
            $this->__string($item['name'], '', $item['errors']);
            foreach ($item['props'] as $index => $n) {
                $callback = array($this, '__' . $n['type']);
                if (is_callable($callback)) {
                    call_user_func($callback, $item['name'], $n, $item['errors']);
                }
            }
        }

        return count($this->ERRORS) === 0;
    }

    private function __setValue($prop, $value)
    {
        array_push($this->DATAS, $prop['value']);
    }

    private function __setDefault($name, $default)
    {
        $this->DATAS[$name] = is_string($default) ? htmlspecialchars($default) : $default;
    }

    private function __setError($name, $value)
    {
        if (!array_key_exists($name, $this->ERRORS)) {
            $this->ERRORS[$name] = [];
        }
        array_push($this->ERRORS[$name], $value);
    }

    private function __getError($key, $errors)
    {
        return array_key_exists($key, $errors) ? $errors[$key] : $this->ERROR_TYPES[$key];
    }

    private function __getErrorWithFormat($key, $value, $errors)
    {
        $n = array_key_exists($key, $errors) ? $errors[$key] : $this->ERROR_TYPES[$key];
        return sprintf($n, $value);
    }

    private function __required($name, $props, $errors)
    {

        /** Burada bir dosya bulundu */
        if ($props['value'] === true && !empty($_FILES[$name])) {
            /** Önceli multiple veya tekil dosya olup olmadığını 
             * Ve boş olup olmadığını kontrol ediyoruz
             */
            if (
                (is_array($_FILES[$name]['tmp_name']) && count($_FILES[$name]['tmp_name']) == 0) ||
                (is_string($_FILES[$name]['tmp_name']) && empty($_FILES[$name]['tmp_name']))
            ) {
                $this->__setError($name, $this->__getError('REQUIRED', $errors));
            }
        }
        /** Bu bir dosya olmayan ve boş bir değerse ve zorunluysa  */
        else if (empty($_POST[$name]) && $props['value'] === true) {
            $this->__setError($name, $this->__getError('REQUIRED', $errors));
        }
    }

    private function __minlength($name, $props, $errors)
    {
        if (!$this->__int($name, $props, $errors)) return;

        if (strlen($_POST[$name]) < $props['value']) {
            $this->__setError($name, $this->__getErrorWithFormat('MIN_LENGTH', $props['value'], $errors));
            $this->__setDefault($name, 0);
            return;
        }

        $this->__setDefault($name, $_POST[$name]);
    }

    private function __maxlength($name, $props, $errors)
    {
        if (!$this->__int($name, $props, $errors)) return;

        if (strlen($_POST[$name]) > $props['value']) {
            $this->__setError($name, $this->__getErrorWithFormat('MAX_LENGTH', $props['value'], $errors));
            $this->__setDefault($name, 0);
            return;
        }

        $this->__setDefault($name, $_POST[$name]);
    }

    private function __minvalue($name, $props, $errors)
    {
        if (!$this->__int($name, $props, $errors)) return;

        if ($_POST[$name] < (int) $props['value']) {
            $this->__setError($name, $this->__getErrorWithFormat('MIN_VALUE', $props['value'], $errors));
            $this->__setDefault($name, 0);
            return;
        }

        $this->__setDefault($name, (int) $_POST[$name]);
    }

    private function __maxvalue($name, $props, $errors)
    {
        if (!$this->__int($name, $props, $errors)) return;

        if ($_POST[$name] > (int) $props['value']) {
            $this->__setError($name, $this->__getErrorWithFormat('MAX_VALUE', $props['value'], $errors));
            $this->__setDefault($name, 0);
            return;
        }

        $this->__setDefault($name, (int) $_POST[$name]);
    }

    private function __string($name, $props, $errors)
    {
        if (empty($_POST[$name])) return;
        $this->__setDefault($name, htmlspecialchars($_POST[$name]));
    }
    private function __int($name, $props, $errors)
    {
        if (!empty($_POST[$name]) && !is_numeric($_POST[$name])) {
            $this->__setError($name, $this->__getError('TYPE_INT', $errors));
            $this->__setDefault($name, 0);
            return false;
        }
        $this->__setDefault($name, (int) $_POST[$name]);
        return true;
    }
    private function __float($name, $props, $errors)
    {
        if (!empty($_POST[$name]) && !is_float($_POST[$name])) {
            $this->__setError($name, $this->__getError('TYPE_FLOAT', $errors));
            $this->__setDefault($name, 0);
            return false;
        }
        $this->__setDefault($name, $_POST[$name]);
        return true;
    }
    private function __bool($name, $props, $errors)
    {
        if (empty($_POST[$name]) || !is_numeric($_POST[$name])) {
            $this->__setError($name, $this->__getError('TYPE_BOOL', $errors));
            $this->__setDefault($name, 0);
            return;
        }

        $this->__setDefault($name, $_POST[$name]);
        return true;
    }

    private function __file($name, $props, $errors)
    {
        $this->FILES[$name] = [];

        $extensions = array_key_exists('extensions', $props) ? $props['extensions'] : [];
        $is_extensions = count($extensions) > 0;

        $add_file = function (
            $filename,
            $file
        ) use (
            $props,
            $extensions,
            $is_extensions,
            $name,
            $errors
        ) {

            $ext = explode('.', $filename);
            $ext = end($ext);
            $valid = !$is_extensions || in_array($ext, $extensions);
            if ($valid) {
                array_push($this->FILES[$name], array(
                    'ext' => $ext,
                    'name' => basename($filename, '.' . $ext),
                    'file' => $file,
                ));
            } else {
                $this->__setError($name, $this->__getErrorWithFormat('FILE_EXTENSIONS', join(',.', $extensions), $errors));
                $this->FILES[$name] = [];
            }
            return $valid;
        };

        if (!empty($_FILES[$name])) {
            $files = $_FILES[$name];
            /**
             * Biz normal yollarla geliştiriciden aldığımız "multiple" değerine bakmadan
             * Sadece isim kontrolüyle multi veya single dosya içeriğini kontrol ediyoruz
             */
            if (is_array($files['tmp_name'])) {
                foreach ($files['tmp_name'] as $index => $tmp) {
                    if (!$add_file($files['name'][$index], $tmp)) {
                    }
                }
            } else {
                $add_file($files['name'], $files['tmp_name']);
            }
        }
    }
}
