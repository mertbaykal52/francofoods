const $ = require("jquery");
require("jquery.easing");

import "./mask";

function validateEmail(email) {
    var re = /^([a-zA-Z]+)([0-9]*)([\.]{1})*([a-zA-Z\_\-0-9]+)\@{1}([a-zA-Z]+[0-9a-zA-Z]*\.)+[a-z]{1,10}$/;
    return re.test(email);
}

$(document).ready(function() {
    $(".phone-mask").mask("(599) 999 99 99");

    $(document).on("submit", ".kayitforum", function(e) {
        e.preventDefault();
        // Kayıt formu seç
        const kayitforum = $(this);
        // Formdaki dataları parse et
        const data = kayitforum.serializeArray();
        // Hatanın yazılacağı nesneyi seç
        const formerror = $(".form-error label");

        // Email ve telefon için aynı kontrol
        let email = null;
        let phone = null;
        // Her bir datanın doluluğunu kontrol et
        const isApprove = data.filter((item) => {
            if (item.name == "email") {
                email = item.value;
            }
            if (item.name == "phone") {
                phone = item.value && item.value.replace(/\D/g, "");
            }
            return !item.value;
        });
        if (isApprove.length > 0) {
            formerror.html("Please fill in all fields");
            return false;
        }

        if (!phone ||
            (phone && phone.length < 10) ||
            (phone && phone.length > 11) ||
            !/\d+/.test(phone)
        ) {
            formerror.html("Phone number is invalid");
            return false;
        } else if (!email || !validateEmail(email)) {
            formerror.html("Please write a valid e-mail address");
            return false;
        }
        formerror.html("Please wait while submitting the form");
        kayitforum.css("pointer-events", "none");
        $.ajax({
            url: "contact.send.php",
            method: "POST",
            data: data,
            dataType: 'json',
            success: function(response) {
                kayitforum.css("pointer-events", "inherit");
                if (response && response.status > 0) {
                    formerror.html("Your information has been sent. We thank you.");
                    kayitforum.closest("form").find("input[type=text], textarea").val("");
                } else {
                    formerror.html(
                        (response && response.message) || "A technical error has occurred"
                    );
                }
            },
            error: function() {
                formerror.html("Something went wrong. Data cannot be sent.");
                kayitforum.css("pointer-events", "inherit");
            },
        });
    });


});