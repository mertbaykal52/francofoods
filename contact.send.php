<?php

require(__DIR__ . "/vendor/phpmailer/phpmailer/src/PHPMailer.php");
require(__DIR__ . "/vendor/phpmailer/phpmailer/src/SMTP.php");


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;



require_once(__DIR__ . '/core/model.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    ////////////////////////////////////////
    $model = new Model();

    $model->set('name', [
        FormType::required(),
    ]);
    $model->set('lastname', [
        FormType::required(),
    ]);
    $model->set('phone', [
        FormType::required(),
    ]);
    $model->set('email', [
        FormType::required(),
    ]);
    $model->set('message', [
        FormType::required(),
    ]);
        if ($model->valid()) {
            $d = $model->getDatas();
            $content = file_get_contents(__DIR__ . '/emails/contact.html');
    
            $content = str_replace('{name}', $d['name'], $content);
            $content = str_replace('{lastname}', $d['lastname'], $content);
            $content = str_replace('{phone}', $d['phone'], $content);
            $content = str_replace('{email}', $d['email'], $content);
            $content = str_replace('{message}', $d['message'], $content);
    
            $mail = new PHPMailer(true);
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = gethostbyname('mail.digitalpanzehir.com');
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->Port = 587;
            $mail->Username = 'dene@digitalpanzehir.com';
            $mail->Password = 'Dn1234';
            $mail->SetFrom($mail->Username, 'Franco Foods');
            $mail->AddAddress('ilker@digitalpanzehir.com', 'Franco Foods');
            //$mail->addCC('', 'İlker Barouh');
            $mail->CharSet = 'UTF-8';
            $mail->Subject = 'Franco Foods Form';
            $mail->MsgHTML($content);
            if ($mail->Send()) {
                $data['status'] = 200;
                $data['message'] = 'Your information has been sent. We thank you.';
            } else {
                $data['status'] = -1;
                $data['message'] = 'The transaction was terminated for a technical reason.';
            }
            echo json_encode(['result' => 'It`s saved', 'extras' => $model->getDatas(), 'status' => 200]);
            exit;
        }

        echo json_encode(["result" => "Geçersiz işlem yürütüldü", 'errors' => $model->getErrors(), 'status' => 500]);
        exit;
        ////////////////////////////////////////
}
