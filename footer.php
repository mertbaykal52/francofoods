<footer>
    <div class="footer">
        <div class="footer-all">

            <div class="footer-all-item">
                <div class="footer-title">
                    <p class="footer-title">HOME PAGE</p>
                </div>

            </div>
            <div class="footer-all-item">
                <div class="footer-title">
                    <p class="footer-title"><a href="about-us">ABOUT US</a></p>
                </div>
                <ul>
                    <li><a href="about-us">About Franco Foods</a></li>
                    <li><a href="milestone">Milestones</a></li>
                    <li><a href="our-mission">Our Missions And Commitments</a></li>
                    <li><a href="quality-management">Quality Management</a></li>

                </ul>

            </div>
            <div class="footer-all-item">
                <div class="footer-title">
                    <p class="footer-title">PRODUCTS</p>
                </div>
                <ul>
                    <li><a href="standard-moistured">Dried Tomatoes</a></li>
                    <li><a href="dried-fruits">Dried Fruits</a></li>
                    <li><a href="spicies-and-herbs">Spicies And Herbs</a></li>

                </ul>

            </div>
            <div class="footer-all-item">
                <div class="footer-title padding-left">
                    <p class="footer-title">CONTACT</p>
                </div>
                <div class="footer-all-contact">
                    <div class="footer-all-contact_image">
                        <img src="assets/images/location-icon.png" alt="location icon" />
                    </div>
                    <a href="https://www.google.com.tr/maps/dir//Tuna,+Franco+Foods,+5501.+Sk.+NO.10%2FA,+35090+Bornova%2F%C4%B0zmir/@38.4298484,27.1853951,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x14b962904fe0eebb:0x3610335505ca69c8!2m2!1d27.1875838!2d38.4298442">
                        <div class="footer-all-contact_desc">
                            <p class="footer-all-contact"><strong style="margin-right: 26px;">Addres</strong>: İZMİR / TURKEY</p>


                        </div>
                    </a>
                </div>
                <div class="footer-all-contact">
                    <div class="footer-all-contact_image">
                        <img src="assets/images/tel-icon.png" alt="telephone" />
                    </div>
                    <div class="footer-all-contact_desc">
                        <a href="tel://+902324214930"><strong style="margin-right: 2px;">Telephone</strong>: +90 232 421 49 30</a>
                    </div>
                </div>
                <div class="footer-all-contact">
                    <div class="footer-all-contact_image">
                        <img src="assets/images/fax-icon.png" alt="fax icon" />
                    </div>
                    <div class="footer-all-contact_desc">
                        <a href="tel://+902324636770"><strong style="margin-right: 55px;">Fax</strong>: +90 232 463 67 70</a>
                    </div>
                </div>
                <div class="footer-all-contact">
                    <div class="footer-all-contact_image">
                        <img src="assets/images/mail-icon.png" alt="mail icon" />
                    </div>
                    <div class="footer-all-contact_desc">
                        <a href="mailto:info@francofoods.com"><strong style="margin-right: 25px;">E-posta</strong>: info@francofoods.com </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="footer-all_bottom">
            <div class="footer-all_bottom-copyright">
                <p>© 2020 Franco Foods</p>
            </div>
          <div class="footer-all_bottom-logo">
              <a href="http://digitalpanzehir.com/"><img src="assets/images/DP-logo.png" alt=""></a>
              
          </div>
        </div>
    </div>


</footer>



<script src="assets/js/swiper-bundle.min.js"></script>
<script src="assets/js/bundle.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 512313500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>
<script>
    $(document).ready(function() {
        $(function() {
            AOS.init();
        });
        window.addEventListener('load', AOS.refresh)
    });
</script>
</body>

</html>